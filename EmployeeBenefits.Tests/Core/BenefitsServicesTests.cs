﻿using EmployeeBenefits.Core;
using EmployeeBenefits.Db.Context;
using EmployeeBenefits.Db.Models;
using EmployeeBenefits.Discounts;
using EmployeeBenefits.Discounts.Factories;
using EmployeeBenefits.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace EmployeeBenefits.Tests.Core
{
    [TestClass]
    public class BenefitsServicesTests
    {
        private Mock<IBenefitsContext> _benefitsContextMock;
        private Mock<IBenefitsDiscountContext> _benefitsDiscountContextMock;
        private Mock<IBenefitsDiscountFactory> _benefitsDiscountFactoryMock;
        private Prospect _prospect;
        private IBenefitsServices _benefitsServices;

        [TestInitialize]
        public void Initialize()
        {
            _benefitsContextMock = new Mock<IBenefitsContext>();
            _benefitsDiscountContextMock = new Mock<IBenefitsDiscountContext>();
            _benefitsDiscountFactoryMock = new Mock<IBenefitsDiscountFactory>();

            _benefitsContextMock
                .Setup(x => x.GetPaychecksPerYear())
                .Returns(26);
            _benefitsContextMock
                .Setup(x => x.GetYearlyCost())
                .Returns(new BenefitYearlyCost
                    {
                        Employee = 1000,
                        Dependent = 500
                    });
            _benefitsDiscountContextMock
                .Setup(x => x.GetNameStartsWithLettersDiscount())
                .Returns(new NameStartsWithLettersDiscount
                {
                    Letters = new char[] { 'A' },
                    Percentage = 10
                });

            _benefitsDiscountFactoryMock
                .Setup(x => x.GetBenefitsDiscount(Enums.Discounts.NameStartsWithLettersDiscount))
                .Returns(new StartsWithLetter(_benefitsDiscountContextMock.Object.GetNameStartsWithLettersDiscount()));

            _benefitsServices = new BenefitsServices(
                _benefitsContextMock.Object,
                _benefitsDiscountFactoryMock.Object);

            _prospect = new Prospect
            {
                Employee = new Employee
                {
                    FirstName = "Albert",
                    LastName = "Smith"
                },
                Dependents = new List<Dependent>
                {
                    new Dependent
                    {
                        FirstName = "Monic",
                        LastName = "Strauss"
                    },
                }
            };
        }

        [TestMethod]
        public void CreateQuote_NoDependents_ValidQuote()
        {
            _prospect.Dependents = null;
            var response = _benefitsServices.CreateQuote(_prospect);

            Assert.AreEqual(900, response.YearlyBenefitsCost);
            Assert.AreEqual(34.62, response.PaycheckDeductions);
            Assert.AreEqual(1965.38, response.NetPaycheckAmount);
        }

        [TestMethod]
        public void CreateQuote_OneDependent_ValidQuote()
        {
            var response = _benefitsServices.CreateQuote(_prospect);

            Assert.AreEqual(1400, response.YearlyBenefitsCost);
            Assert.AreEqual(53.85, response.PaycheckDeductions);
            Assert.AreEqual(1946.15, response.NetPaycheckAmount);
        }

        [TestMethod]
        public void CreateQuote_MultipleDependents_ValidQuote()
        {
            _prospect.Dependents = new List<Dependent>
            {
                new Dependent
                    {
                        FirstName = "Monic",
                        LastName = "Strauss"
                    },
                new Dependent
                    {
                        FirstName = "Ace",
                        LastName = "Ventura"
                    },
                new Dependent
                    {
                        FirstName = "Azul",
                        LastName = "Wall"
                    },
            };

            var response = _benefitsServices.CreateQuote(_prospect);

            Assert.AreEqual(2300, response.YearlyBenefitsCost);
            Assert.AreEqual(88.46, response.PaycheckDeductions);
            Assert.AreEqual(1911.54, response.NetPaycheckAmount);
        }
    }
}
