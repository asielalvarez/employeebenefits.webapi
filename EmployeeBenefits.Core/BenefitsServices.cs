﻿using EmployeeBenefits.Core.Dto;
using EmployeeBenefits.Core.Utilities;
using EmployeeBenefits.Db.Context;
using EmployeeBenefits.Db.Models;
using EmployeeBenefits.Discounts;
using EmployeeBenefits.Discounts.Factories;
using EmployeeBenefits.Discounts.Models;
using EmployeeBenefits.Discounts.Extensions;
using EmployeeBenefits.Domain;
using System.Collections.Generic;
using System.Linq;

namespace EmployeeBenefits.Core
{
    public class BenefitsServices : IBenefitsServices
    {
        private readonly IBenefitsDiscount _benefitsDiscount;
        private readonly BenefitYearlyCost _benefitYearlyCost;
        private readonly int _paychecksPerYear;

        public BenefitsServices(
            IBenefitsContext benefitsContext, 
            IBenefitsDiscountFactory benefitsDiscountFactory)
        {
            _benefitYearlyCost = benefitsContext.GetYearlyCost();
            _paychecksPerYear = benefitsContext.GetPaychecksPerYear();
            _benefitsDiscount = benefitsDiscountFactory.GetBenefitsDiscount(Enums.Discounts.NameStartsWithLettersDiscount);
        }

        public BenefitsQuote CreateQuote(Prospect prospect)
        {            
            var prospectDiscount = _benefitsDiscount.GetTotalYearBenefitsDiscounts(prospect);
            var yearlyBenefitsCost = CalculateTotalYearBenefitsCost(prospect, prospectDiscount);
            var paycheckDeductions = PaycheckCalculations.PaycheckDeductions(new List<double> { yearlyBenefitsCost }, _paychecksPerYear);
            var paycheckNetAmount = PaycheckCalculations.PaycheckNetAmount(prospect.Employee.Salary, paycheckDeductions, _paychecksPerYear);

            return new BenefitsQuote
            {
                YearlyBenefitsCost = yearlyBenefitsCost.RoundUpCurrency(),
                PaycheckDeductions = paycheckDeductions.RoundUpCurrency(),
                NetPaycheckAmount = paycheckNetAmount.RoundUpCurrency()
            };
        }

        private double CalculateTotalYearBenefitsCost(Prospect prospect, ProspectDiscount prospectDiscount)
        {
            var yearlyBenefitsCost = PriceAfterDiscount(_benefitYearlyCost.Employee, prospectDiscount.Employee.Percentage);

            yearlyBenefitsCost += prospect?.Dependents
                ?.Select((d, idx) => PriceAfterDiscount(_benefitYearlyCost.Dependent, prospectDiscount.Dependents.ElementAt(idx).Percentage))
                .Sum() ?? 0;

            return yearlyBenefitsCost;
        }

        private double PriceAfterDiscount(double originalPrice, double discountPercentage) =>
            originalPrice - (originalPrice * discountPercentage / 100);
    }
}
