﻿using EmployeeBenefits.Core.Dto;
using EmployeeBenefits.Domain;

namespace EmployeeBenefits.Core
{
    public interface IBenefitsServices
    {
        BenefitsQuote CreateQuote(Prospect prospect);
    }   
}
