﻿using System.Collections.Generic;

namespace EmployeeBenefits.Core.Utilities
{
    public static class PaycheckCalculations
    {
        public static double PaycheckDeductions(List<double> deductions, int paychecksPerYear)
        {
            double deductionsPerCheck = 0;

            if(deductions != null)
            {
                foreach (var deduction in deductions)
                {
                    deductionsPerCheck += deduction;
                }
            }

            return deductionsPerCheck / paychecksPerYear;
        }

        public static double PaycheckNetAmount(double employeeSalary, double paycheckDeductions, int paychecksPerYear) =>
            employeeSalary / paychecksPerYear - paycheckDeductions;
    }
}
