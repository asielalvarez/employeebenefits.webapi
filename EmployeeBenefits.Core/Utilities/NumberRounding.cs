﻿using System;

namespace EmployeeBenefits.Core.Utilities
{
    public static class NumberRounding
    {
        public static double RoundUpCurrency(this double number) => Math.Round(number, 2, MidpointRounding.AwayFromZero);
    }
}
