﻿namespace EmployeeBenefits.Core.Dto
{
    public class BenefitsQuote
    {
        public double YearlyBenefitsCost { get; set; }
        public double PaycheckDeductions { get; set; }
        public double NetPaycheckAmount { get; set; }
    }
}
