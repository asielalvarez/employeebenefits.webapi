﻿using EmployeeBenefits.Db.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmployeeBenefits.Domain
{
    public class Prospect
    {
        [Required]
        public Employee Employee { get; set; }
        public IEnumerable<Dependent> Dependents { get; set; }
    }
}
