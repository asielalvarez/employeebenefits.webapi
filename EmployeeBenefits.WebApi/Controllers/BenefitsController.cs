﻿using EmployeeBenefits.Core;
using EmployeeBenefits.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeBenefits.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BenefitsController : ControllerBase
    {
        private readonly IBenefitsServices _benefitsServices;
       
        public BenefitsController(IBenefitsServices benefitsServices)
        {
            _benefitsServices = benefitsServices;
        }

        [HttpPost("Quote")]
        public IActionResult CreateQuote(Prospect prospect)
        {
            return Created("", _benefitsServices.CreateQuote(prospect));
        }
    }
}
