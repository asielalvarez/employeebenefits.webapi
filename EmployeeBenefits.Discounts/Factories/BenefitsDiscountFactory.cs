﻿using EmployeeBenefits.Db.Context;
using EmployeeBenefits.Domain;

namespace EmployeeBenefits.Discounts.Factories
{
    public class BenefitsDiscountFactory : IBenefitsDiscountFactory
    {
        private readonly IBenefitsDiscountContext _benefitsContext;

        public BenefitsDiscountFactory(IBenefitsDiscountContext benefitsContext)
        {
            _benefitsContext = benefitsContext;
        }

        public IBenefitsDiscount GetBenefitsDiscount(Enums.Discounts discount)
        {
            switch (discount)
            {
                case Enums.Discounts.NameStartsWithLettersDiscount:                    
                    return new StartsWithLetter(_benefitsContext.GetNameStartsWithLettersDiscount());
                default:
                    return null;
            }
        }
    }
}
