﻿using EmployeeBenefits.Domain;

namespace EmployeeBenefits.Discounts.Factories
{
    public interface IBenefitsDiscountFactory
    {
        IBenefitsDiscount GetBenefitsDiscount(Enums.Discounts discount);
    }
}