﻿using EmployeeBenefits.Discounts.Models;
using EmployeeBenefits.Domain;
using System.Linq;

namespace EmployeeBenefits.Discounts.Extensions
{
    public static class BenefitsDiscountExtensions
    {
        public static ProspectDiscount GetTotalYearBenefitsDiscounts(this IBenefitsDiscount benefitDiscount, Prospect prospect)
        {
            var prospectDiscount = new ProspectDiscount
            {
                Employee = new CustomerDiscount
                {
                    Customer = prospect.Employee,
                    Percentage = benefitDiscount.GetDiscountPercentage(prospect.Employee)
                }
            };
            prospectDiscount.Dependents = prospect.Dependents
                ?.Select(d => new CustomerDiscount
                {
                    Customer = d,
                    Percentage = benefitDiscount.GetDiscountPercentage(d)
                });

            return prospectDiscount;
        }
    }
}
