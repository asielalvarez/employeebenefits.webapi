﻿using System.Collections.Generic;

namespace EmployeeBenefits.Discounts.Models
{
    public class ProspectDiscount
    {
        public CustomerDiscount Employee { get; set; }
        public IEnumerable<CustomerDiscount> Dependents { get; set; }
    }
}
