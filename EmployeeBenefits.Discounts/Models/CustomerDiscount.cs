﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Discounts.Models
{
    public class CustomerDiscount
    {
        public BasePerson Customer { get; set; }
        public double Percentage { get; set; }
    }
}
