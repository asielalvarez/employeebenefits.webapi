﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Discounts
{
    public interface IBenefitsDiscount
    {
        double GetDiscountPercentage(Employee employee);
        double GetDiscountPercentage(Dependent dependent);
    }
}
