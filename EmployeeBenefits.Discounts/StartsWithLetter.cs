﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Discounts
{
    public class StartsWithLetter : IBenefitsDiscount
    {
        private readonly NameStartsWithLettersDiscount _nameStartsWithLettersDiscount;
        public StartsWithLetter(NameStartsWithLettersDiscount nameStartsWithLettersDiscount)
        {
            _nameStartsWithLettersDiscount = nameStartsWithLettersDiscount;
        }

        public double GetDiscountPercentage(Employee employee) => GetPercentage(employee);

        public double GetDiscountPercentage(Dependent dependent) => GetPercentage(dependent);

        private double GetPercentage(BasePerson person)
        {
            foreach(var letter in _nameStartsWithLettersDiscount.Letters)
            {
                if (person.FirstName.ToUpper().StartsWith(letter.ToString().ToUpper()))
                {
                    return _nameStartsWithLettersDiscount.Percentage;
                }
            }

            return 0;
        }
    }
}
