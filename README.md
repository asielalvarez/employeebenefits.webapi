# EmployeeBenefits.WebApi

This is a web api project to handle employee benefits.

### .WebApi
This is where controllers and most of the setup for the application lives at. It includes an endpoint to create a benefits quote.

### .Core
WebApi relies on Core for business logic. Core includes a method to create the benefits quote.

### .Discounts
This class library holds logic specific to benefit's discounts.

### .Db
This is where we would access the database using sql queries, LINQ, etc.

### .Domain
A light class library project that holds DTOs and Enums. It helps us avoid circular dependency.

### .Test
This is where we inlcude our unit tests.