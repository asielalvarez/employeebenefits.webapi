﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeBenefits.Db.Models
{
    public abstract class BasePerson
    {
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}
