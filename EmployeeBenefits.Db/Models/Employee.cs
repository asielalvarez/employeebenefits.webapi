﻿namespace EmployeeBenefits.Db.Models
{
    public class Employee : BasePerson
    {
        public double Salary { get; set; } = 52000;
    }
}
