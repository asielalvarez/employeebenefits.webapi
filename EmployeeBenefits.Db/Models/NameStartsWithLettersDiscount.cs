﻿namespace EmployeeBenefits.Db.Models
{
    public class NameStartsWithLettersDiscount
    {
        public char[] Letters { get; set; }
        public double Percentage { get; set; }
    }
}
