﻿namespace EmployeeBenefits.Db.Models
{
    public class BenefitYearlyCost
    {
        public double Employee { get; set; }
        public double Dependent { get; set; }
    }
}