﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Db.Context
{
    public class BenefitsDiscountContext : IBenefitsDiscountContext
    {
        public NameStartsWithLettersDiscount GetNameStartsWithLettersDiscount() => 
            new NameStartsWithLettersDiscount
            {
                Letters = new char[] { 'A' },
                Percentage = 10
            };
    }
}
