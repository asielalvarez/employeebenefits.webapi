﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Db.Context
{
    public interface IBenefitsDiscountContext
    {
        NameStartsWithLettersDiscount GetNameStartsWithLettersDiscount();
    }
}
