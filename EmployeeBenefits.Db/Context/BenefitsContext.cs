﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Db.Context
{
    public class BenefitsContext : IBenefitsContext
    {
        public BenefitYearlyCost GetYearlyCost() => new BenefitYearlyCost
        {
            Employee = 1000,
            Dependent = 500
        };

        public int GetPaychecksPerYear() => 26;
    }
}
