﻿using EmployeeBenefits.Db.Models;

namespace EmployeeBenefits.Db.Context
{
    public interface IBenefitsContext
    {
        BenefitYearlyCost GetYearlyCost();
        int GetPaychecksPerYear();
    }
}
